Require Import Coq.Program.Equality.
Require Import List ZArith.
Require Import Arith.
Require Import Psatz.
Require Import Omega.
Import ListNotations.
Close Scope nat_scope.
Open Scope Z_scope.
Require Import String.
Open Scope string_scope.

Definition var := string.
Definition val := Z.
Definition var_eq := string_dec.

(** * 1 - Le langage While *)

(** Le langage While est un langage impératif minimaliste. Les valeurs
manipulées dans ce langage sont des entiers naturels [val := nat] et les
variables sont représentées par un identifiant (de type [string]). *)

(** ** Les expressions, conditions et instructions *)
(** Les expressions [expr] peuvent être des constantes [Const n], des variables
[Var n], ou bien des opérations binaires ([Add], [Mul], [Sub] sur d'autres
expressions). *)

Inductive expr : Set :=
| Const (n: val)
| Var (v: var)
| Add (e1 e2: expr)
| Mul (e1 e2: expr)
| Sub (e1 e2: expr).

(** On a aussi des conditions [cond] qui seront utilisées pour les branchements
conditionnels ([if then else] et [while]). Ces conditions peuvent être l'égalité
entre deux expressions [Eq], une infériorité stricte [Lt], la conjonction ou la
disjonction de deux conditions [And] et [Or] ou bien la négation d'une condition
[Not]. *)

Inductive cond : Set :=
| Eq (e1 e2: expr)
| Lt (e1 e2: expr)
| And (c1 c2: cond)
| Or (c1 c2: cond)
| Not (c: cond).

(** Les instructions [stmt] sont les suivantes :
 - [Skip] : ne rien faire
 - [Assign v e] : stocke le résultat de l'évaluation de [e] dans la variable [v]
 - [Seq s1 s2] : faire [s1], puis [s2]
 - [If c s1 s2] : si [c] est vrai, faire [s1], sinon faire [s2]
 - [While c s] : tant que [c] est vrai, faire [s]
 *)

(** ** Évaluation des expressions et conditions  *)

(** Pour évaluer les expressions, nous avons besoin d'un *environnement* qui
associe à chaque variable sa valeur. On utilisera pour cela le type [state]
définit ci-dessous. Les fonctions d'évaluation des expressions [eval_expr] et
des conditions [eval_cond] sont standard. *)

Definition state := var -> val.

(** [update_env e x z] crée un nouvel environnement à partir de [e] en associant
la valeur [z] à la variable [x]. *)

Definition update_state (e: state) (x: var) (z: val) :=
  fun y => if var_eq y x then z else e y.

Fixpoint eval_expr (env: state) (e: expr) : val :=
  match e with
  | Const n => n
  | Var v => env v
  | Add e1 e2 => (eval_expr env e1) + (eval_expr env e2)
  | Mul e1 e2 => (eval_expr env e1) * (eval_expr env e2)
  | Sub e1 e2 => (eval_expr env e1) - (eval_expr env e2)
  end.

Compute
  let s := fun _ => 0 in
  let s := update_state s "x" 12 in
  let s := update_state s "y" 5 in
  let e := Mul (Const 3) (Add (Var "x") (Mul (Const 12) (Sub (Var "x") (Var "y")))) in
  eval_expr s e.


Fixpoint eval_cond (env: state) (c: cond) : bool :=
  match c with
  | Eq e1 e2 => Z.eqb (eval_expr env e1) (eval_expr env e2)
  | Lt e1 e2 => Z.ltb (eval_expr env e1) (eval_expr env e2)
  | And c1 c2 => andb (eval_cond env c1) (eval_cond env c2)
  | Or c1 c2 => orb (eval_cond env c1) (eval_cond env c2)
  | Not c => negb (eval_cond env c)
  end.

Fixpoint eval_condP (env: state) (c: cond) : Prop :=
  match c with
  | Eq e1 e2 => (eval_expr env e1) = (eval_expr env e2)
  | Lt e1 e2 => (eval_expr env e1) < (eval_expr env e2)
  | And c1 c2 => and (eval_condP env c1) (eval_condP env c2)
  | Or c1 c2 => or (eval_condP env c1) (eval_condP env c2)
  | Not c => not (eval_condP env c)
  end.

Lemma contrapositive : forall P Q: Prop, (P->Q)->((~Q)->(~P)).
intros.
unfold not. intro. apply H0. apply H. apply H1.
Qed.

Lemma eval_cond_true:
  forall env c,
    eval_condP env c <->
    eval_cond env c = true.
Proof.
intros. induction c.
* split.
- simpl. apply <- Z.eqb_eq.
- simpl. apply Z.eqb_eq.
* split.
- apply <- Z.ltb_lt.
- simpl. apply Z.ltb_lt.
* split.
- simpl. intro. destruct H.
  destruct IHc1; destruct IHc2.
 rewrite (H1 H). rewrite (H3 H0). auto.
- simpl. intro. apply andb_prop in H. destruct H.
destruct IHc1; destruct IHc2. split.
+ apply H2. assumption.
+ apply H4. assumption.
* split.
- simpl. intro. apply Bool.orb_true_intro. destruct H.
+ left. apply IHc1. assumption.
+ right. apply IHc2. assumption.
- simpl. intro. apply Bool.orb_true_elim in H. destruct H.
+ left. apply IHc1. assumption.
+ right. apply IHc2. assumption.
* split.
- simpl. unfold not. intros. apply not_iff_compat in IHc.
apply Bool.eq_true_not_negb. apply IHc. unfold not. apply H.
- simpl. unfold not. intros. apply not_iff_compat in IHc.
apply <- IHc.
+ apply Bool.negb_true_iff in H. unfold not. intro. rewrite H1 in H. discriminate H.
+ apply H0.
Qed.

Lemma eval_cond_false:
  forall env c,
    ~ eval_condP env c <->
    eval_cond env c = false.
Proof.
intros env c. 
rewrite <- Bool.not_true_iff_false.
split.
* apply contrapositive. apply eval_cond_true.
* apply contrapositive. apply eval_cond_true.
Qed.


Lemma eval_cond_dec:
  forall env c, {eval_condP env c} + {~ eval_condP env c}.
Proof.
intros. induction c.
* simpl. apply Z.eq_dec.
* simpl. apply Z_lt_dec.
* simpl. destruct IHc1; destruct IHc2.
- left. auto.
- right. unfold not. intro. destruct H. apply n. assumption.
- right. unfold not. intro. destruct H. apply n. assumption.
- right. unfold not. intro. destruct H. apply n. assumption.
* simpl. destruct IHc1; destruct IHc2.
- left; left; assumption.
- left; left; assumption.
- left; right; assumption.
- right. unfold not; intro. destruct H.
+ apply n; assumption.
+ apply n0; assumption.
* simpl. destruct IHc.
- right. unfold not. intros. apply H. assumption.
- left. assumption.
Defined.

(*
intros. destruct (eval_cond env c) eqn:E.
* left. apply <- eval_cond_true. assumption.
* right. apply eval_cond_false. assumption.
Defined. *)

(*Admitted. *)                       (* à remplacer par Defined. quand vous aurez fini. *)
(* Defined permet de rendre les définitions *transparentes*, et pourront donc
être évaluées par la commande Compute. *)

Compute
  let s := fun _ => 0 in
  let s := update_state s "x" 12 in
  let s := update_state s "y" 5 in
  let e := Mul (Const 3) (Add (Var "x") (Mul (Const 12) (Sub (Var "x") (Var "y")))) in
  eval_cond_dec s (Eq e (Const 288)).



Inductive stmt  :=
| Skip
| Assign (v: var) (e: expr)
| Seq (s1 s2: stmt)
| If (c: cond) (s1 s2: stmt)
| While (c: cond) (I: state -> Prop) (s: stmt)
.

