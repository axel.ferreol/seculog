Require Import Coq.Program.Equality.
Require Import List ZArith.
Require Import Arith.
Require Import Psatz.
Require Import Omega.
Import ListNotations.
Open Scope nat_scope.
Close Scope Z_scope.
Require Import String.
Open Scope string_scope.
Require Import While Tactics.
Require Import WhileBigStep.
Require Import Lia.


Fixpoint exec_prog (fuel: nat) (env: state) (i: stmt) : option state :=
match fuel with
| O => None
| S n =>
  match i with
  | Skip => Some env
  | Assign x e => Some (update_state env x (eval_expr env e))
  | Seq s1 s2 => 
    match exec_prog n env s1 with
    | None => None
    | Some env' => exec_prog n env' s2
    end
  | If c s1 s2 => 
      if eval_cond env c 
        then exec_prog n env s1 else exec_prog n env s2
  | While c Inv s => 
      if eval_cond env c then
        match exec_prog n env s with
        | None => None
        | Some env' => exec_prog n env' (While c Inv s)
        end
      else Some env
  end
end.



Definition p1 :=
  Seq (Assign "x" (Const 10%Z))
      (Seq (Assign "sum" (Const 0%Z))
           (While (Not (Eq (Var "x") (Const 0%Z)))
                  (fun _ => True)
                  (Seq
                     (Assign "sum" (Add (Var "sum") (Var "x")))
                     (Assign "x" (Sub (Var "x") (Const 1%Z)))
                  )
           )
      ).

(* Le calcul suivant devrait retourner [Some 55]. *)
Compute
  option_map (fun env => env "sum")
  (exec_prog 30 (fun _ => 0%Z) p1).


Theorem exec_prog_bigstep:
  forall fuel s i s',
    exec_prog fuel s i = Some s' ->
    bigstep s i s'.
Proof. 
intro. induction fuel; intros.
* simpl in H. discriminate H.
* simpl in H. destruct i.
- injection H as H. rewrite H. constructor.
- injection H as H. rewrite <- H. constructor.
- destruct (exec_prog fuel s i1) eqn:E.
+ econstructor.
** eapply IHfuel. apply E.
** eapply IHfuel. apply H.
+ discriminate H.
- destruct (eval_cond s c) eqn:E.
+ eapply bigstep_if_true.
** apply <-eval_cond_true. apply E.
** eapply IHfuel. apply H.
+ eapply bigstep_if_false.
** apply <-eval_cond_false. apply E.
** eapply IHfuel. apply H.
- destruct (eval_cond s c) eqn:E.
+ destruct (exec_prog fuel s i) eqn:E'.
** eapply bigstep_while_true.
-- apply <-eval_cond_true. apply E.
-- eapply IHfuel. apply E'.
-- eapply IHfuel. apply H.
** discriminate H.
+ injection H as H. rewrite <- H. eapply bigstep_while_false.
apply <-eval_cond_false. apply E.
Qed.


Lemma exec_prog_more_fuel:
  forall f s i s',
    exec_prog f s i = Some s' ->
    forall f',
      f' >= f ->
      exec_prog f' s i = Some s'.
Proof.
intro fuel. induction fuel; intros.
* simpl in H. discriminate H.
* destruct f' eqn:E.
- inversion H0.
- assert (n >= fuel). { lia. } 
  destruct i; simpl in H; simpl.
+ assumption.
+ assumption.
+ destruct (exec_prog fuel s i1) eqn:E'.
** rewrite (IHfuel s i1 s0 E' n H1).
  rewrite (IHfuel s0 i2 s' H n H1). reflexivity.
** discriminate H.
+ destruct (eval_cond s c).
** eapply IHfuel; assumption.
** eapply IHfuel; assumption.
+ destruct (eval_cond s c).
** destruct(exec_prog fuel s i) eqn:E1.
-- rewrite (IHfuel s i s0 E1 n H1). eapply IHfuel; assumption.
-- discriminate H.
** assumption.
Qed.


Theorem bigstep_exec_prog:
  forall s i s',
    bigstep s i s' ->
    exists fuel,
      exec_prog fuel s i = Some s'.
Proof.
intros. induction H.
* exists 1. simpl. reflexivity.
* exists 1. simpl. reflexivity.
* destruct IHbigstep1. destruct IHbigstep2.
  exists (S (max x x0)).
  simpl.
  rewrite (exec_prog_more_fuel x env s1 env').
- rewrite (exec_prog_more_fuel x0 env' s2 env'').
+ reflexivity.
+ assumption.
+ lia.
- assumption.
- lia.
* destruct IHbigstep.
  exists (S x). simpl. 
  assert (eval_cond env c = true). { apply eval_cond_true; assumption. }
  rewrite H2. assumption.
* destruct IHbigstep.
  exists (S x). simpl. 
  assert (eval_cond env c = false). { apply eval_cond_false; assumption. }
  rewrite H2. assumption.
* destruct IHbigstep1. destruct IHbigstep2.
  exists (S (max x x0)).
  simpl.
  destruct (eval_cond env c) eqn:E.
- rewrite (exec_prog_more_fuel x env s env').
+ rewrite (exec_prog_more_fuel x0 env' (While c I s) env'').
** reflexivity.
** assumption.
** lia.
+ assumption.
+ lia.
- apply eval_cond_false in E. apply E in H. destruct H.
* exists 1. simpl. destruct (eval_cond env c) eqn:E.
- apply eval_cond_true in E. apply H in E. destruct E.
- reflexivity.
Qed.