Require Import Coq.Program.Equality.
Require Import List ZArith.
Require Import Arith.
Require Import Psatz.
Require Import Omega.
Import ListNotations.
Open Scope nat_scope.
Close Scope Z_scope.
Require Import String.
Open Scope string_scope.
Require Import While Tactics.
Require Import WhileSmallStep WhileBigStep.


Lemma star_seq:
  forall s1 env1 env2,
    star s1 env1 Skip env2 ->
    forall s2 s3 env3,
      star s2 env2 s3 env3 ->
      star (Seq s1 s2) env1 s3 env3.
Proof.
  intros s1 env1 env2 Hstar.
  dependent induction Hstar; simpl; intros.
* eapply star_step.
- apply step_seq_skip.
- apply H.
* eapply star_step.
- eapply step_seq. apply H.
- apply IHHstar.
+ reflexivity.
+ apply H0.
Qed.

Theorem bigstep_star:
  forall i env env',
    bigstep env i env' ->
    star i env Skip env'.
Proof.
intros. induction H.
* apply star_refl.
* eapply star_step.
- apply step_assign.
- apply star_refl.
* eapply star_seq.
- apply IHbigstep1.
- apply IHbigstep2.
* eapply star_step.
- eapply step_if_true. assumption.
- assumption.
* eapply star_step.
- eapply step_if_false. assumption.
- assumption.
* eapply star_step.
- eapply step_while_true. assumption.
- eapply star_seq.
+ apply IHbigstep1.
+ apply IHbigstep2.
* eapply star_step.
- eapply step_while_false. assumption.
- apply star_refl.
Qed.


Lemma step_bigstep:
  forall i env i' env',
    step i env i' env' ->
    forall env'',
      bigstep env' i' env'' ->
      bigstep env i env''.
Proof.
intros i en i' env' s. induction s.
* intros. inversion H. apply bigstep_assign.
* intros. apply bigstep_if_true.
- assumption.
- assumption.
* intros. apply bigstep_if_false.
- assumption.
- assumption.
* intros. inversion H0.
eapply bigstep_while_true.
- assumption.
- apply H4.
- assumption.
* intros. inversion H0. eapply bigstep_while_false. rewrite <- H3. assumption.
* intros. inversion H. eapply bigstep_seq.
- apply IHs. apply H3.
- assumption.
* intros. eapply bigstep_seq.
- apply bigstep_skip.
- assumption.
Qed.

Theorem star_bigstep:
  forall i env env',
    star i env Skip env' ->
    bigstep env i env'.
Proof.
intros. dependent induction H.
* apply bigstep_skip.
* eapply step_bigstep.
- apply H.
- apply IHstar. reflexivity.
Qed.